<u>Witchcraft</u>
===================
***
<h3>More will be added to this Readme as I get a working version of Witchcraft!<br>
Please bare with me, I am a lone developer currently also learning Java programming!<br><br>
I would like to mention that this is as spoken a complete remake, no code or assets were taken
from the original mod as EVERYTHING is completely rewritten!<br>

If there is any issues concerning copyright concerns, 
please contact the author, and it will be dealt with immediately!</h3>
***

<h3>DISCLAIMER</h3>
<h4>Credits for the idea and what made this possible:</h4>
<ol>
1. A Huge part of this project has to go to Emoniph for making <a href="https://www.curseforge.com/minecraft/mc-mods/witchery">Witchery</a> 1.7.10!<br>
2. Thank you to the Forge team for keeping Modded Minecraft's heart pumping!
</ol>
<h4>Credits to the Development Team:</h4>
<ol>
1. Seristic, The author and main developer of Witchcraft!<br>
</ol>

***

<h4>What's the aim for Witchcraft?</h4>
Witchcraft is an entire remake of Witchery back in the days of Minecraft 1.7.10! <br>
When it comes to Witchcraft, the vision we have is to bring Witchcraft back but **BETTER**. <br>
We fully intend on bringing back a lot of old features that made Witchery stand out from the rest,
but we have also got a lot of ideas for Witchcraft that I think would make very good additions to an 
already amazing mod! <br><br>

<h4>So, what will change, and what will remain the same?</h4>
Well most of the original features of Witchery will remain the same, as the items and blocks themselves, the Rituals(Circle Magic),
Poppets, Dream Weaving, Brewing, Infusions, Familiars and Fetishes. Some items will have new textures/models to bring them up to date with
the newer version look.<br>

You can expect to see a lot of new features in the future, this list will constantly be changing as time goes on. 
We hope that conspiring with the devil, WILL cause some consequences, 
as making a deal with the devil shouldn't always go well. A full list of ideas will be published as soon as we get a nice list
going for the mod, so you all have something to
look forward to!<br>
***
Demonic Desires | All Rights Reserved © 2022 | Not affiliated with "Emoniph" or "MinecraftForge" Or "Mojang"
