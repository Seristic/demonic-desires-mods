<u>Witchcraft Terms of Use</u>
===================
***
<ol>
1. If you would like to add this mod to a modpack, 
please do, all I ask from you is to just let me know CurseForge is fine. You don't need a response from me,
I want to keep track of who is using my mod.<br>
2. You must give VERY visible credit to the author(s): Seristic <br>
3. You cannot under and circumstances claim ANY part of Witchcraft. Please read the below!<br>
4. You must provide a link back to the Original CurseForge Page. <br>
5. You and or anyone cannot make any commercial gain from the mod itself or the modpack(Excludes Servers). <br>
6. If you are caught voiding this terms of use, then I will request that you remove Witchcraft from
the modpack until further noticed.  <br>
</ol>

***
TERMS AND CONDITIONS
0. USED TERMS<br>
MOD - modification, plugin, a piece of software that interfaces with the Minecraft client to extend, add, change or remove original capabilities.<br>
MOJANG - Mojang AB<br>
OWNER - Seristic, Original author(s) of the MOD. Under the copyright terms accepted when purchasing Minecraft (http://www.minecraft.net/copyright.jsp) the OWNER has full rights over their MOD despite use of MOJANG code.<br>
USER - End user of the mod, person installing the mod.<br>
1. 
***
1. LIABILITY <br>
    THIS MOD IS PROVIDED 'AS IS' WITH NO WARRANTIES, IMPLIED OR OTHERWISE. THE OWNER OF THIS MOD TAKES NO RESPONSIBILITY FOR ANY DAMAGES INCURRED FROM THE USE OF THIS MOD. THIS MOD ALTERS FUNDAMENTAL PARTS OF THE MINECRAFT GAME, PARTS OF MINECRAFT MAY NOT WORK WITH THIS MOD INSTALLED. ALL DAMAGES CAUSED FROM THE USE OR MISUSE OF THIS MOD FALL ON THE USER.<br><br>
 
2. USE<br>
    Use of this MOD to be installed, manually or automatically, is given to the USER without restriction.<br><br>
 
3. REDISTRIBUTION <br>
    This MOD may only be distributed where uploaded, mirrored, or otherwise linked to by the OWNER solely. All mirrors of this mod must have advance written permission from the OWNER. ANY attempts to make money off of this MOD (selling, selling modified versions, adfly, sharecash, etc.) are STRICTLY FORBIDDEN, and the OWNER may claim damages or take other action to rectify the situation.<br><br>
 
4. DERIVATIVE WORKS/MODIFICATION <br>
    This Mod may not be decompiled, reproduced and/or modified in any way, under any circumstance, without explicit permission from the Author(s): Seristic.<br><br>

5. MODPACKS <br>
    Witchcraft may be used within Modpacks that comply with the above, so long as they have contacted the author(s): Seristic.
    All modpacks must ensure that they follow the terms of conditions, if this is failed then the Owner reserves the rights to
    revoke the permission that was once granted given that a notice of thirty (30) days have been given.
    
