<u>Witchcraft Feature List</u>
===================
***
<h3>What old features will be returning from Witchery 1.7.10:</h3>
<ol>
1. Poppets <br>
2. Dream Weaving <br>
3. Circle Magic <br>
4. Brewing <br>
5. Infusions <br>
6. Familiars <br>
7. Fetishes <br>

If any old features were missed, please tell us, we will add them here, if they will return.
</ol>

<h3>What new features you can expect to see in the future:<br>
(And the probability of this happening.)</h3>
<ol>
1. Sanity System | 100% | <br>
2. Harm the Witch | 100%<br>
3. Summoning Fails | 30%<br>
4. Wizards/Witchcraft And Wizardry | 50%<br>
5. Realm Tears | 90%<br>
6. Whole new Dimension for Witches | 60%<br>
7. More to come! <br>

</ol>
If you would like to suggest a feature, that you think makes sense to be added<br>
a discord will be available where you can then post your suggestion. Link will be provided.<br>
Also within the Discord, will be a channel that will show the progress of Witchcraft go
check it out to get an idea of what we are up to!

***

<h2>Santiy System</h2>
{ INSERT CONTENT }

<h2>Harm the Witch</h2>
{ INSERT CONTENT }

<h2>Summoning Fails</h2>
{ INSERT CONTENT }

<h2>Wizards/Witchcraft And Wizardry</h2>
{ INSERT CONTENT }

<h2>Realm Tears</h2>
{ INSERT CONTENT }

<h2>Whole new Dimension for Witches</h2>
{ INSERT CONTENT }

<h2>Honor a God, a Goddess or both!</h2>
{ INSERT CONTENT }

<h2>Herbalism</h2>
{ INSERT CONTENT }

<h2>Divination</h2>
{ INSERT CONTENT }

<h2>Animism</h2>
{ INSERT CONTENT }

***