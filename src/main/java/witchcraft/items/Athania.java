package witchcraft.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemSword;
import witchcraft.Witchcraft;

public class Athania extends ItemSword {

    public Athania() {
        super(Witchcraft.myToolMaterial);

        this.setRegistryName("wc_athania");
        this.setUnlocalizedName("wc_athania");
        this.setCreativeTab(CreativeTabs.COMBAT);
    }
}
