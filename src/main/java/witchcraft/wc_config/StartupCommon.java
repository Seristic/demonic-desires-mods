package witchcraft.wc_config;

import net.minecraft.item.Item;

public class StartupCommon
{
    public static Item itemtestblock;

    public static void preInitCommon()
    {
        WCConfig.preInit();
        System.out.println("Witchcraft: myInteger=" + WCConfig.myInteger
                + "; myBoolean=" + WCConfig.myBoolean
                + "; myString=" + WCConfig.myString);
        System.out.println("Witchcraft: myDouble=" + WCConfig.myDouble
                + "; myColour=" + WCConfig.myColour);
        System.out.print("Witchcraft: myIntList=");
        for (int value : WCConfig.myIntList) {
            System.out.print(value + "; ");
        }
        System.out.println();

    }

    public static void initCommon()
    {
    }

    public static void postInitCommon()
    {
    }

}