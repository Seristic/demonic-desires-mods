package witchcraft;

import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;
import witchcraft.items.Athania;

@Mod(modid = Witchcraft.MODID, name = Witchcraft.NAME, useMetadata = false, acceptedMinecraftVersions = "[1.12, 1.12.2]",
guiFactory = Witchcraft.GUIFACTORY)


public class Witchcraft {
    public static final String MODID = "witchcraft";
    public static final String GUIFACTORY = "witchcraft.wc_configuration.WCGuiFactory";
    public static final String NAME = "Witchcraft";

    @Mod.Instance(Witchcraft.MODID)
    public static Witchcraft instance;

    public static Logger logger;

    public static Item.ToolMaterial myToolMaterial;
    public static Item mySword;

    @SidedProxy(clientSide = "witchcraft.ClientOnlyProxy", serverSide = "witchcraft.DedicatedServerProxy")
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();

        myToolMaterial = EnumHelper.addToolMaterial("Athania", 0, 0, 0F, 0.5F, 0);
        mySword = new Athania();

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }
    public static String prepareModID(String name) {return MODID + ":" + name;}

}
