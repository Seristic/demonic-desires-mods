package witchcraft;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

public class ClientOnlyProxy extends CommonProxy {

    public void preInit() {
        //read config first
//        witchcraft.wc_config.StartupCommon.preInitCommon();

//        witchcraft.testblock01.StartupCommon.preInitCommon();

    }

    public void init()
    {
        witchcraft.wc_config.StartupCommon.initCommon();

//        witchcraft.testblock01.StartupCommon.initCommon();

    }

    public void postInit()
    {
        witchcraft.wc_config.StartupCommon.postInitCommon();

//        witchcraft.testblock01.StartupClientOnly.postInitClientOnly();

    }

    @Override
    public boolean playerIsInCreativeMode(EntityPlayer player) {
        if (player instanceof EntityPlayerMP) {
            EntityPlayerMP entityPlayerMP = (EntityPlayerMP) player;
            return entityPlayerMP.interactionManager.isCreative();
        }
        return false;
    }

    @Override
    public boolean isDedicatedServer() {return true;}
}